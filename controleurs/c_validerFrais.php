<?php

include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
switch ($action) {
    case 'selectionnerVisiteur': {
            $lesVisiteurs = $pdo->getLesVisiteursDisponibles();
            $lesCles = array_keys($lesVisiteurs);
            $visiteurASelectionner = $lesCles[0];
            include("vues/v_listeVisiteurs.php");
            break;
        }
    case 'selectionnerMois': {
            $idVisiteur = $_REQUEST['lstVisiteur'];
            $lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
            // Afin de sélectionner par défaut le dernier mois dans la zone de liste
            // on demande toutes les clés, et on prend la première,
            // les mois étant triés décroissants
            $lesCles = array_keys($lesMois);
            $moisASelectionner = $lesCles[0];
            include("vues/v_listeMoisVisiteur.php");
            break;
        }
    case 'voirEtatFrais': {
            $leMois = $_REQUEST['lstMois'];
            $mois = substr($leMois, 0, 6);
            $idVisiteur = substr($leMois, 6);
            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
            $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $mois);
            $numAnnee = substr($mois, 0, 4);
            $numMois = substr($mois, 4, 2);
            $libEtat = $lesInfosFicheFrais['libEtat'];
            $montantValide = $lesInfosFicheFrais['montantValide'];
            $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
            $dateModif = $lesInfosFicheFrais['dateModif'];
            $dateModif = dateAnglaisVersFrancais($dateModif);
            include("vues/v_validationFrais.php");
            $lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
            $moisASelectionner = $mois;
            include("vues/v_listeMoisVisiteur.php");
            break;
        }
    case 'validationFraisForfait': {
            $leMois = $_REQUEST['validation'];
            $etat = substr($leMois, 0, 2);
            $mois = substr($leMois, 2, 6);
            $idVisiteur = substr($leMois, 8);
            echo $mois;
            $pdo->majEtatFicheFrais($idVisiteur, $mois, $etat);
            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
            $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $mois);
            $numAnnee = substr($mois, 0, 4);
            $numMois = substr($mois, 4, 2);
            $libEtat = $lesInfosFicheFrais['libEtat'];
            $montantValide = $lesInfosFicheFrais['montantValide'];
            $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
            $dateModif = $lesInfosFicheFrais['dateModif'];
            $dateModif = dateAnglaisVersFrancais($dateModif);
            include("vues/v_validationFrais.php");
            $lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
            $moisASelectionner = $mois;
            include("vues/v_listeMoisVisiteur.php");
            break;
        }
    case 'validationHorsForfait': {
            $delimiter = "-";
            $valider = explode($delimiter, $_REQUEST['validation']);
            $idLigneHorsForfait = $valider[0];
            $idVisiteur = $valider[1];
            $mois = $valider[2];
            $numAnnee = substr($valider[2], 0, 4);
            $numMois = substr($valider[2], 3, 2);
            $pdo->refusHorsForfait($idLigneHorsForfait, $mois);
            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
            $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $mois);
            $libEtat = $lesInfosFicheFrais['libEtat'];
            $montantValide = $lesInfosFicheFrais['montantValide'];
            $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
            $dateModif = $lesInfosFicheFrais['dateModif'];
            $dateModif = dateAnglaisVersFrancais($dateModif);
            include("vues/v_validationFrais.php");
            $lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
            $moisASelectionner = $mois;
            include("vues/v_listeMoisVisiteur.php");
            break;
        }
}
?>
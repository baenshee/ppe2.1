<?php

include("vues/v_sommaire.php");
$action = $_REQUEST['action'];
switch ($action) {
    case 'selectionnerVisiteur': {
            $lesVisiteurs = $pdo->getLesVisiteursDisponibles();
            $lesCles = array_keys($lesVisiteurs);
            $visiteurASelectionner = $lesCles[0];
            include("vues/v_listeVisiteursSuivi.php");
            break;
        }
    case 'selectionnerMois': {
            $idVisiteur = $_REQUEST['lstVisiteur'];
            $lesMois = $pdo->getLesMoisDisponibles($idVisiteur);
            // Afin de sélectionner par défaut le dernier mois dans la zone de liste
            // on demande toutes les clés, et on prend la première,
            // les mois étant triés décroissants
            $lesCles = array_keys($lesMois);
            $moisASelectionner = $lesCles[0];
            include("vues/v_listeMoisVisiteurSuivi.php");
            break;
        }
    case 'voirSuivi': {
            $leMois = $_REQUEST['lstMois'];
            $mois = substr($leMois, 0, 6);
            $idVisiteur = substr($leMois, 6);
            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur, $mois);
            $lesFraisForfait = $pdo->getLesFraisForfait($idVisiteur, $mois);
            $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($idVisiteur, $mois);
            $numAnnee = substr($mois, 0, 4);
            $numMois = substr($mois, 4, 2);
            $libEtat = $lesInfosFicheFrais['libEtat'];
            $montantValide = $lesInfosFicheFrais['montantValide'];
            $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
            $dateModif = $lesInfosFicheFrais['dateModif'];
            $dateModif = dateAnglaisVersFrancais($dateModif);
            include("vues/v_suiviPaiement.php");

            break;
        }
}
?>
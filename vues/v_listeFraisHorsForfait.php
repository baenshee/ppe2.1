<article>
    <div class="col-md-9 contenu" id="contenu">﻿
        <div class="well validerFrais">
            <p>Descriptif des éléments hors forfait</p>
            <table class="table listeLegere">
                <tr>
                    <th class="date">Date</th>
                    <th class="libelle">Libellé</th>  
                    <th class="montant">Montant</th>  
                    <th class="action">&nbsp;</th>              
                </tr>
                <?php
                foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                    $libelle = $unFraisHorsForfait['libelle'];
                    $date = $unFraisHorsForfait['date'];
                    $montant = $unFraisHorsForfait['montant'];
                    $id = $unFraisHorsForfait['id'];
                    ?>		
                    <tr>
                        <td> <?php echo $date ?></td>
                        <td><?php echo $libelle ?></td>
                        <td><?php echo $montant ?></td>
                        <td>
                            <a href="index.php?uc=gererFrais&action=supprimerFrais&idFrais=<?php echo $id ?>" 
                               onclick="return confirm('Voulez-vous vraiment supprimer ce frais?');">Supprimer ce frais</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>	  
            </table>
        </div>

        <div class="validerFrais well">
            <form class="form-horizontal" action="index.php?uc=gererFrais&action=validerCreationFrais" method="post">
                <fieldset>
                    <legend>Nouvel élément hors forfait</legend>
                    <div class="corpsForm form-group">

                        <label class="control-label col-xs-3" for="txtDateHF">Date (jj/mm/aaaa) :</label>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" id="txtDateHF" name="dateFrais" value=""  />
                        </div>

                        <label class="control-label col-xs-3" for="txtLibelleHF">Libellé :</label>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" id="txtLibelleHF" name="libelle" value="" />
                        </div>

                        <label class="control-label col-xs-3" for="montant">Montant :</label>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" id="montant" name="montant" value="" />
                        </div>

                    </div>
                </fieldset>
                <div class="piedForm row">
                    <input class="col-md-4 btn btn-success btn-lg" id="ok" type="submit" value="Valider" />
                    <input class="col-md-4 col-md-offset-4 btn btn-danger btn-lg" id="annuler" type="reset" value="Effacer" />
                </div>
            </form>
        </div>

        <div class="validerFrais well">
            <form class="form-horizontal" action="index.php?uc=gererFrais&action=ajoutJustificatif" method="post">
                <fieldset>
                    <legend>Ajout de justificatifs</legend>
                    <div class="corpsForm form-group">

                        <label class="control-label col-xs-3" for="nombre">Nombre :</label>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" id="nombre" name="libelle" value="" />
                        </div>

                        <label class="control-label col-xs-3" for="montant">Montant total :</label>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" id="montant" name="montant" value="" />
                        </div>
                    </div>
                </fieldset>
                <div class="piedForm row">
                    <input class="col-md-4 btn btn-success btn-lg" id="ok" type="submit" value="Valider" />
                    <input class="col-md-4 col-md-offset-4 btn btn-danger btn-lg" id="annuler" type="reset" value="Effacer" />
                </div>
            </form>
        </div>

    </div>
</article>
<article>
    <div class="col-md-9 contenu" id="contenu">
        <h2>Fiche de frais du mois <?php echo $numMois . "-" . $numAnnee ?> :</h2>
        <div class="well encadre">
            Etat : <?php echo $libEtat ?> depuis le <?php echo $dateModif ?> <br> Montant validé : <?php echo $montantValide ?>
            <h3>Frais compris dans le forfait</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th>Libelle</th>
                        <th>Montant</th>
                        <th>Quantité</th>
                        <th>Montant Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($lesFraisForfait as $unFraisForfait) { ?>
                        <tr>
                            <?php
                            $libelle = $unFraisForfait['libelle'];
                            $montant = $unFraisForfait['montant'];
                            $quantite = $unFraisForfait['quantite'];
                            ?>
                            <th> <?php echo $libelle ?></th>
                            <td> <?php echo $montant ?></td>
                            <td> <?php echo $quantite ?></td>
                            <td> <?php echo $montant * $quantite ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="well validerFrais">
            <p>Descriptif des éléments hors forfait: </br>
                <?php echo $nbJustificatifs ?> justificatifs reçus 
            </p>
            <table class="table listeLegere">
                <tr>
                    <th class="date">Date</th>
                    <th class="libelle">Libellé</th>
                    <th class='montant'>Montant</th>                
                </tr>
                <?php
                foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                    $date = $unFraisHorsForfait['date'];
                    $libelle = $unFraisHorsForfait['libelle'];
                    $montant = $unFraisHorsForfait['montant'];
                    ?>
                    <tr>
                        <td><?php echo $date ?></td>
                        <td><?php echo $libelle ?></td>
                        <td><?php echo $montant ?></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
</article>
<article>
    <div class="col-md-9 contenu" id="contenu">
        <h2>Le mois : </h2>
        <form class="well form-horizontal" action="index.php?uc=suivieFrais&action=voirSuivi" method="post">
            <fieldset>
                <legend>Mois à sélectionner</legend>
                <div class="corpsForm">
                    <label for="lstMois" accesskey="n">Mois : </label>
                    <select id="lstMois" name="lstMois">
                        <?php
                        foreach ($lesMois as $unMois) {
                            $mois = $unMois['mois'];
                            $numAnnee = $unMois['numAnnee'];
                            $numMois = $unMois['numMois'];
                            if ($mois == $moisASelectionner) {
                                ?>
                                <option selected value="<?php echo $mois . $idVisiteur ?>"><?php echo $numMois . "/" . $numAnnee ?> </option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $mois . $idVisiteur ?>"><?php echo $numMois . "/" . $numAnnee ?> </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="piedForm row">
                    <input class="col-md-4 btn btn-success btn-lg" id="ok" type="submit" value="Valider" />
                    <input class="col-md-4 col-md-offset-4 btn btn-danger btn-lg" id="annuler" type="reset" value="Effacer" />
                </div>
            </fieldset>
        </form>
    </div>
</article>
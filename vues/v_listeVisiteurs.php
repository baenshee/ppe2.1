<article>
    <div class="col-md-9 contenu" id="contenu">
        <h2>Les visiteurs</h2>
        <form class="well form-horizontal" action="index.php?uc=validerFrais&action=selectionnerMois" method="post">
            <fieldset>
                <legend>Visiteur à sélectionner</legend>
                <div class="corpsForm">
                    <label for="lstVisiteur" accesskey="n">Visiteur : </label>
                    <select id="lstVisiteur" name="lstVisiteur">
                        <?php
                        echo $visiteurASelectionner;
                        foreach ($lesVisiteurs as $unVisiteur) {
                            $visiteur = $unVisiteur['id'];
                            $nom = $unVisiteur['nom'];
                            $prenom = $unVisiteur['prenom'];
                            if ($visiteur == $visiteurASelectionner) {
                                ?>
                                <option selected value="<?php echo $visiteur ?>"><?php echo $prenom . " " . $nom ?> </option>
                                <?php
                            } else {
                                ?>
                                <option value="<?php echo $visiteur ?>"><?php echo $prenom . " " . $nom ?> </option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="piedForm row">
                    <input class="col-md-4 btn btn-success btn-lg" id="ok" type="submit" value="Valider" />
                    <input class="col-md-4 col-md-offset-4 btn btn-danger btn-lg" id="annuler" type="reset" value="Effacer" />
                </div>
            </fieldset>
        </form>
    </div>
</article>
﻿    <!-- Division pour le sommaire -->
<nav>
    <div class="well col-sm-3 menuGauche table-bordered" id="menuGauche">
        <div class="menuTitre" id="infosUtil">
            <h2>Menu</h2>
        </div>  
        <ul class="menuList" id="menuList">
            <div class="menuNomFonction" >
                <li class="smenu">
                    Visiteur:
                    <?php echo $_SESSION['prenom'] . "   " . $_SESSION['nom'] . "</br>"; ?>
                    Fonction:
                    <?php
                    if ($_SESSION['comptable'] == 1) {
                        echo "Comptable";
                    } else {
                        echo "Visiteur";
                    }
                    ?> 
                </li>   
            </div>
            <?php
            if ($_SESSION['comptable'] == 1) {
                ?>
                <li class="smenu">
                    <a href="index.php?uc=validerFrais&action=selectionnerVisiteur" title="Valider des fiches de frais ">Validation des fiches de frais</a>
                </li>
                <li class="smenu">
                    <a href="index.php?uc=suivieFrais&action=selectionnerVisiteur" title="Paiement des fiches de frais">Suivi du paiement des fiches de frais</a>
                </li>
                <?php
            } else {
                ?>
                <li class="smenu">
                    <a href="index.php?uc=gererFrais&action=saisirFrais" title="Saisie fiche de frais ">Saisie fiche de frais</a>
                </li>
                <li class="smenu">
                    <a href="index.php?uc=etatFrais&action=selectionnerMois" title="Consultation de mes fiches de frais">Mes fiches de frais</a>
                </li>
                <?php
            }
            ?>
            <div class="menuDeconnexion">
                <li class="smenu">
                    <a href="index.php?uc=connexion&action=deconnexion" title="Se déconnecter">Déconnexion</a>
                </li>
            </div>
        </ul>
    </div>
</nav>
<section class="row col-xs-9">
<article>
    <div class="col-md-9 contenu" id="contenu">
        <h2>Fiche de frais du mois <?php echo $numMois . "-" . $numAnnee ?> : </h2>
        <div class="well encadre">
            <p>
                Etat : <?php echo $libEtat ?> depuis le <?php echo $dateModif ?> <br> Montant validé : <?php echo $montantValide ?>
            </p>
            <h3>Frais compris dans le forfait :</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th>Libelle</th>
                        <th>Montant</th>
                        <th>Quantité</th>
                        <th>Montant Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($lesFraisForfait as $unFraisForfait) {
                        ?>
                        <tr>
                            <?php
                            $libelle = $unFraisForfait['libelle'];
                            $montant = $unFraisForfait['montant'];
                            $quantite = $unFraisForfait['quantite'];
                            ?>
                            <th> <?php echo $libelle ?></th>
                            <th> <?php echo $montant ?></th>
                            <th> <?php echo $quantite ?></th>
                            <th> <?php echo $montant * $quantite ?></th>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class=" well validerFrais">
            <p>Descriptif des éléments hors forfait: </br>
                <?php echo $nbJustificatifs ?> justificatifs reçus 
            </p>
            <table class="table listeLegere">
                <!--<form action="index.php?uc=validerFrais&action=validationFraisForfait" method="post">-->
                <tr>
                    <th class="date">Date</th>
                    <th class="libelle">Libellé</th>
                    <th class='montant'>Montant</th>
                    <th class="valide" style="color: black">Valider</th>
                </tr>
                <?php
                foreach ($lesFraisHorsForfait as $unFraisHorsForfait) {
                    $date = $unFraisHorsForfait['date'];
                    $libelle = $unFraisHorsForfait['libelle'];
                    $montant = $unFraisHorsForfait['montant'];

                    $idLigneHorsForfait = $unFraisHorsForfait['id'];
                    $idVisiteur = $unFraisHorsForfait['idVisiteur'];
                    $mois = $unFraisHorsForfait['mois'];
                    ?>
                    <tr>
                        <td><?php echo $date ?></td>
                        <td><?php echo $libelle ?></td>
                        <td><?php echo $montant ?></td>
                        <td>
                            <a href="index.php?uc=validerFrais&action=validationHorsForfait&validation=<?php echo $idLigneHorsForfait . '-' . $idVisiteur . '-' . $mois ?>">
                                <button class=" btn btn-danger btn-sm" type="button"  value="">Refuser</button>
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
                <!--</form>-->
            </table>
            <a href="index.php?uc=validerFrais&action=validationFraisForfait&validation=<?php echo "VA" . $numAnnee . $numMois . $idVisiteur ?>">
                <button class=" btn btn-success btn-lg" type="button"  value="">Valider</button>
            </a>
        </div>
    </div>
</article>
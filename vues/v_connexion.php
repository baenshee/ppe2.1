﻿<div id="entete">
    <form role="form" method="POST" class="col-md-4 col-md-offset-4 well form-horizontal" action="index.php?uc=connexion&action=valideConnexion">

        <div class="form-group">
            <label class="control-label col-xs-5" for="login">Identifiant :</label>
            <div class="col-xs-7">
                <input class="form-control" id="login" type="text" name="login" maxlength="45">
            </div>

            <label class="control-label col-xs-5" for="mdp" >Mot de passe :</label>
            <div class="col-xs-7">
                <input class="form-control" id="mdp" type="password" name="mdp" maxlength="45">
            </div>
        </div>

        <input class="btn btn-success" id="btn1" type="submit" value="Connexion" name="valider">
    </form>
</div>
<article>
    <div class="col-md-9 validerFrais" id="contenu">
        <h2>Renseigner ma fiche de frais du mois <?php echo $numMois . "-" . $numAnnee ?></h2>
        <form class="well form-horizontal" method="POST"  action="index.php?uc=gererFrais&action=validerMajFraisForfait">
            <fieldset>
                <legend>Eléments forfaitisés</legend>
                <?php
                foreach ($lesFraisForfait as $unFrais) {
                    $idFrais = $unFrais['idfrais'];
                    $libelle = $unFrais['libelle'];
                    $quantite = $unFrais['quantite'];
                    ?> 
                    <div class="form-group">
                        <label class="control-label col-xs-3" for="idFrais"><?php echo $libelle ?> :</label>
                        <div class="col-xs-9">
                            <input class="form-control" type="text" id="idFrais" name="lesFrais[<?php echo $idFrais ?>]" size="10" maxlength="5" value="<?php echo $quantite ?>" >
                        </div>
                    </div>
                    <?php
                }
                ?>
            </fieldset>
            <div class="piedForm row">
                <input class="col-md-4 btn btn-success btn-lg" id="ok" type="submit" value="Valider" />
                <input class="col-md-4 col-md-offset-4 btn btn-danger btn-lg" id="annuler" type="reset" value="Effacer" />
            </div>
        </form>
    </div>
</article>
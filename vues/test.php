
﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>Intranet du Laboratoire Galaxy-Swiss Bourdin</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link href="./styles/styles.css" rel="stylesheet" type="text/css" />
        <link href="./styles/bootstrap-3.1.1-dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="./styles/bootstrap-3.1.1-dist/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="header">
            <div class="container header">
                <div class="row" id="logo">
                    <div class="col-md-12 degrade">
                        <img id="imgLogoGSB" src="./images/logo_gsb.png" alt="Laboratoire Galaxy - Swiss Bourdin" title="Laboratoire Galaxy - Swiss Bourdin"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pageCorps">
            <div class="row" id="page">
                <section>﻿    <!-- Division pour le sommaire -->
                    <nav>
                        <div class="well col-md-3 menuGauche table-bordered" id="menuGauche">
                            <div class="menuTitre" id="infosUtil">
                                <h2>Menu</h2>
                            </div>  
                            <ul class="menuList" id="menuList">
                                <div class="menuNomFonction" >
                                    <li class="smenu">
                                        Visiteur:
                                        Bob   Nob</br>                    Fonction:
                                        Comptable 
                                    </li>   
                                </div>
                                <li class="smenu">
                                    <a href="index.php?uc=validerFrais&action=selectionnerVisiteur" title="Valider des fiches de frais ">Validation des fiches de frais</a>
                                </li>
                                <li class="smenu">
                                    <a href="index.php?uc=suivieFrais&action=selectionnerVisiteur" title="Paiement des fiches de frais">Suivi du paiement des fiches de frais</a>
                                </li>
                                <div class="menuDeconnexion">
                                    <li class="smenu">
                                        <a href="index.php?uc=connexion&action=deconnexion" title="Se déconnecter">Déconnexion</a>
                                    </li>
                                </div>
                            </ul>
                        </div>
                    </nav>
                    <div class="emptyNav"></div>
                    <section><br />
                        <font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
                                <tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: lstMois in C:\wamp\www\ppe2.1\controleurs\c_validerFrais.php on line <i>25</i></th></tr>
                                <tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
                                <tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
                                <tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0012</td><td bgcolor='#eeeeec' align='right'>147120</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='C:\wamp\www\ppe2.1\index.php' bgcolor='#eeeeec'>..\index.php<b>:</b>0</td></tr>
                                <tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0209</td><td bgcolor='#eeeeec' align='right'>247600</td><td bgcolor='#eeeeec'>include( <font color='#00bb00'>'C:\wamp\www\ppe2.1\controleurs\c_validerFrais.php'</font> )</td><td title='C:\wamp\www\ppe2.1\index.php' bgcolor='#eeeeec'>..\index.php<b>:</b>28</td></tr>
                            </table></font>
                        <article>
                            <div class="col-md-9 contenu" id="contenu">
                                <h2>Fiche de frais du mois - : </h2>
                                <div class="well encadre">
                                    <p>
                                        Etat :  depuis le // <br> Montant validé :             </p>
                                    <h3>Frais compris dans le forfait :</h3>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Libelle</th>
                                                <th>Montant</th>
                                                <th>Quantité</th>
                                                <th>Montant Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <div class=" well validerFrais">
                                    <p>Descriptif des éléments hors forfait: </br>
                                        justificatifs reçus 
                                    </p>
                                    <table class="table listeLegere">
                                        <!--<form action="index.php?uc=validerFrais&action=validationFraisForfait" method="post">-->
                                        <tr>
                                            <th class="date">Date</th>
                                            <th class="libelle">Libellé</th>
                                            <th class='montant'>Montant</th>
                                            <th class="valide" style="color: black">Valider</th>
                                        </tr>
                                        <!--</form>-->
                                    </table>
                                    <a href="index.php?uc=validerFrais&action=validationFraisForfait&validation=VA">
                                        <button class="btn-default btn btn-success btn-lg" type="button"  value="">Valider</button>
                                    </a>
                                </div>
                            </div>
                        </article><article>
                            <div class="col-md-9 contenu" id="contenu">
                                <h2>Le mois : </h2>
                                <form class="well form-horizontal" action="index.php?uc=validerFrais&action=voirEtatFrais" method="post">
                                    <legend>Mois à sélectionner</legend>
                                    <div class="corpsForm">
                                        <label for="lstMois" accesskey="n">Mois : </label>
                                        <select id="lstMois" name="lstMois">
                                        </select>
                                    </div>
                                    <div class="piedForm row">
                                        <input class="col-md-4 btn-default btn btn-success btn-lg" id="ok" type="submit" value="Valider" />
                                        <input class="col-md-4 col-md-offset-4 btn btn-default btn-danger btn-lg" id="annuler" type="reset" value="Effacer" />
                                    </div>
                                </form>
                            </div>
                        </article>
                        <!-- Division pour le pied de page -->
                    </section></div></div>
        <footer class="container footer" id="footer"></footer>
    </body>
    <script type="text/javascript" src="./js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="./styles/bootstrap-3.1.1-dist/js/bootstrap.js"></script>
</html>


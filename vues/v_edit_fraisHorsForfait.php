<div class="col-md-9 contenu" id="contenu">
        <h3>Frais Hors-Forfait</h3>
        <table class="table-bordered">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Libelle</th>
                    <th>Montant</th>
                    <th>Effacer</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $lesLignes = $ficheActuelle->getFraisHorsForfait();
                if ($lesLignes != NULL) {
                    foreach ($lesLignes as $ligne) {
                        echo ('<tr><td>' . $ligne->getDate() . '</td><td>' . $ligne->getLibelle() .
                        '</td><td>' . $ligne->getMontant() .
                        '</td><td><a class="btn" href="index.php?uc=fiche&action=supprimerLigneHorsForfait&idLigne=' . $ligne->getId() . '&ficheId=' . $ficheActuelle->getIdVisiteur() . '&ficheMois=' . $ficheActuelle->getMois() . '">Supprimer</a>.</td></tr>');
                    }
                    if (DEBUGMOD) {
                        echo "LES LIGNES";
                        var_dump($lesLignes);
                    }
                }
                ?>   
            <form method="POST" action="index.php?uc=fiche&action=ajoutLigneHorsForfait">
                <tr>
                    <td>
                        <select id="jour" class="jour"  name="jour">
                            <?php
                            for ($i = 1; $i <= 31; $i++) {
                                if ($i < 0) {
                                    $i = '0' . $i;
                                }
                                echo('<option value="' . $i . '">' . $i . '</option>');
                            }
                            ?>    
                        </select>
                        <select id="mois" class="moisDeci" name="mois">
                            <?php
                            for ($i = 1; $i <= 12; $i++) {
                                if ($i < 0) {
                                    $i = '0' . $i;
                                }
                                echo('<option value="' . $i . '">' . $i . '</option>');
                            }
                            ?>    
                        </select>
                        <select id="annee" class="annee" name="annee">
                            <?php
                            echo('<option value="' . (date('Y') - 1) . '">' . (date('Y') - 1) . '</option>');
                            echo('<option value="' . date('Y') . '">' . date('Y') . '</option>');
                            ?>    
                        </select>
                    </td>
                    <td>
                        <input id="libelle" type="text" name="libelle"  size="30" maxlength="100">
                    </td>
                    <td>
                        <input id="montant" type="text" name="montant"  size="30" maxlength="100">
                    </td>
                    <td>
                        <input type="hidden" name="ficheMois" value="<?php echo($ficheActuelle->getMois()); ?>">
                        <input type="hidden" name="ficheId" value="<?php echo($ficheActuelle->getIdVisiteur()); ?>">
                        <input type="submit" value="Valider" name="valider">
                    </td>
                </tr>
            </form>
            </tbody>
        </table>
</div>
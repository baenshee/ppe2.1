﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
    <head>
        <title>Intranet du Laboratoire Galaxy-Swiss Bourdin</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link href="./styles/styles.css" rel="stylesheet" type="text/css" />
        <link href="./styles/bootstrap-3.1.1-dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="./styles/bootstrap-3.1.1-dist/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <div class="header">
            <div class="container header">
                <div class="row" id="logo">
                    <div class="col-md-12 degrade">
                        <img id="imgLogoGSB" src="./images/logo_gsb.png" alt="Laboratoire Galaxy - Swiss Bourdin" title="Laboratoire Galaxy - Swiss Bourdin"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pageCorps">
            <div class="row col-xs-12" id="page">
                